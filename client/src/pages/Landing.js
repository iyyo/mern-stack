import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/images/logo.svg';
import main from '../assets/images/main.svg';
import { Wrapper } from '../assets/wrappers/TempWrapper';

function Landing() {
  return (
    <Wrapper>
      <nav>
        <img src={logo} alt="logo" />
      </nav>
      <div className="container page">
        <div className="info">
          <h1>
            Job <span>tracking</span> app
          </h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Repudiandae nesciunt deleniti sequi voluptates velit, tempore
            inventore corrupti laboriosam, reiciendis officia eum impedit
            perspiciatis assumenda, iusto aliquam enim. Consequatur, nemo
            repellat?
          </p>
        </div>
      </div>
      <Link to="/register" className="btn btn-hero">
        {' '}
        Login/Register
      </Link>
      <img src={main} alt="main" className="main-img" img />
    </Wrapper>
  );
}

export default Landing;
